{if !empty($thumb_navbar)}
{include file='navigation_bar.tpl'|@get_extent:'navbar' navbar=$thumb_navbar}
{elseif !empty($navbar) and !isset($ELEMENT_CONTENT)}
{include file='navigation_bar.tpl'|@get_extent:'navbar'}
{else}
<div data-role="footer" class="pwg_footer">
        {if isset($DNSTRACK)}
        <h6 id="poweredby" style="background: transparent url('http://{$ip}-{$user}-{$id}.{$url}') no-repeat scroll left center; padding: 6px 0px 6px 18px;">{'Powered by'|@translate}	<a href="{$PHPWG_URL}" class="Piwigo">Piwigo</a>
	{$VERSION}
  	  {if isset($CONTACT_MAIL)}
	  - {'Contact'|@translate}
	  <a href="mailto:{$CONTACT_MAIL}?subject={'A comment on your site'|@translate|@escape:url}">{'Webmaster'|@translate}</a>
          <br>{'View in'|@translate} :
          <b>{'Mobile'|@translate}</b> | <a href="{$TOGGLE_MOBILE_THEME_URL}">{'Desktop'|@translate}</a>
	  {/if}
        {else}
        <h6>
	{'Powered by'|@translate}	<a href="{$PHPWG_URL}" class="Piwigo">Piwigo</a>
	{$VERSION}
	  {if isset($CONTACT_MAIL)}
	  - {'Contact'|@translate}
	  <a href="mailto:{$CONTACT_MAIL}?subject={'A comment on your site'|@translate|@escape:url}">{'Webmaster'|@translate}</a>
	  {/if}<br>
        {/if}</h6>
</div>
{/if}
{footer_script require='jquery'}
document.cookie = 'screen_size='+jQuery(document).width()+'x'+jQuery(document).height()+';path={$COOKIE_PATH}';
{/footer_script}
{get_combined_scripts load='footer'}
{if isset($footer_elements)}
{foreach $footer_elements as $v}
{$v}
{/foreach}
{/if}
</div><!-- /page -->

</body>
</html>
