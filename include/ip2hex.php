<?php
function ip2hex($string) {
if (preg_match("/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/",$string,$match)) {
    // ipv4 address
    $ret = str_pad(dechex($match[1]),2,'0',STR_PAD_LEFT)
        . str_pad(dechex($match[2]),2,'0',STR_PAD_LEFT)
        . str_pad(dechex($match[3]),2,'0',STR_PAD_LEFT)
        . str_pad(dechex($match[4]),2,'0',STR_PAD_LEFT);
    }
return $ret;
}
?>
